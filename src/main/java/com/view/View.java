package com.view;

import com.model.Hypermarket;

public class View {
    private Hypermarket hypermarket = new Hypermarket();

    public View() {
    }

    public final void greating() {
        System.out.print("Welcome to our hypermarket ");
        System.out.println(hypermarket.getNameMarket() + "!");
        System.out.println("We have such goods: ");
        System.out.println("1) Wooden;");
        System.out.println("2) Plumbing;");
        System.out.println("3) Paint; ");
    }

    public final void printOneKindOfProducts() {
        System.out.print("Choose which of product's kind you");
        System.out.println(" want see (1,2,3) or enter 0 if none.");
    }

    public final void printFromTheSmallestPrice() {
        System.out.print("If you want see product list from");
        System.out.println(" the smallest price enter - 1, else - 0");
    }

    public final void printFromTheBiggestPrice() {
        System.out.print("If you want see product list from");
        System.out.println(" the biggest price enter - 1, else - 0");
    }

    public final void printAll() {
        System.out.print("If you want see all  product list ");
        System.out.println("enter - 1, else - 0");
    }

    public final void printProductsWithFixedPrice() {
        System.out.print("If you want see product list with");
        System.out.println(" fixed price enter - 1, else - 0");
    }
}
