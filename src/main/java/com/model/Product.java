package com.model;


public class Product {
    private  String kind;
    private  String name;
    private  int price;

    public Product(final String pkind, final String pname, final int pprice) {
        this.kind = pkind;
        this.name = pname;
        this.price = pprice;
    }

    public final String getKind() {
        return kind;
    }


    public final int getPrice() {
        return price;
    }


    @Override
    public final int hashCode() {
        int result = name.indexOf(name.length() - 1)
                * kind.indexOf(kind.length() - 1) * price;
        return result;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Product p = (Product) obj;
        return (p.kind == this.kind && p.name
                == this.name && p.price == this.price);
    }

    @Override
    public final String toString() {
        return "{" + kind + ", " + name + ", " + price + '}';
    }
}
