package com.model;

import java.util.ArrayList;


public class Hypermarket {
    private ArrayList<Product> products = new ArrayList<Product>();
    private static String nameMarket = "Builder";

    public Hypermarket() {
    }


    public final String getNameMarket() {
        return nameMarket;
    }

    public final ArrayList<Product> addProducts() {

        Product one = new Product("Wooden", "Chair", 700);
        products.add(one);
        Product two = new Product("Wooden", "Table", 3550);
        products.add(two);
        Product three = new Product("Wooden", "Desk", 1250);
        products.add(three);
        Product four = new Product("Wooden", "Door", 2330);
        products.add(four);
        Product five = new Product("Plumbing", "Tap", 350);
        products.add(five);
        Product six = new Product("Plumbing", "Bowl", 4550);
        products.add(six);
        Product seven = new Product("Plumbing", "Washbasin", 2900);
        products.add(seven);
        Product eight = new Product("Plumbing", "Bath", 6250);
        products.add(eight);
        Product nine = new Product("Paint", "Red", 120);
        products.add(nine);
        Product ten = new Product("Paint", "Black", 100);
        products.add(ten);
        Product eleven = new Product("Paint", "White", 100);
        products.add(eleven);
        return products;
    }

    public final ArrayList<Product> searchingFixedPrice(final int fixedPrice) {
        ArrayList<Product> fixedProductPrice = new ArrayList<Product>();
        for (Product product : products) {
            if (product.getPrice() == fixedPrice) {
                fixedProductPrice.add(product);
            }
        }
        if (fixedProductPrice.size() == 0) {
            System.out.println("We don't have products with such price.");
        }
        return fixedProductPrice;
    }

    public final Product[] sortingFromTheSmallestPrice() {
        Product[] array = new Product[products.size()];
        Product k;
        for (int i = 0; i < products.size(); i++) {
            array[i]=products.get(i);
        }
        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < array.length-i; j++) {
                if (array[j].getPrice() > array[j+1].getPrice()) {
                    k=array[j];
                    array[j] = array[j+1];
                    array[j+1]=k;
                }
            }
        }
        return array;
    }

    public final void sortingFromTheBiggestPrice() {
        for (int i = sortingFromTheSmallestPrice().length-1; i >=0; i--) {
            System.out.println(sortingFromTheSmallestPrice()[i] + " ");
        }
    }

    public final ArrayList<Product> searchingOneKindProducts(final String kind) {
        ArrayList<Product> oneKindProducts = new ArrayList<Product>();
        for (Product p : products) {
            if (p.getKind().equals(kind)) {
                oneKindProducts.add(p);
            }
        }
        return oneKindProducts;
    }

    @Override
    public final int hashCode() {
        int result = nameMarket.indexOf(nameMarket.length() - 2)
                * nameMarket.indexOf(nameMarket.length() - 3);
        return result;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Hypermarket h = (Hypermarket) obj;
        return (h.nameMarket == this.nameMarket);
    }

    @Override
    public final String toString() {
        return "{" + nameMarket + '}';
    }
}
