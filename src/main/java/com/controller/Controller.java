package com.controller;

import com.model.Hypermarket;
import com.view.View;

import java.util.Scanner;

public final class Controller {
    private static Hypermarket market = new Hypermarket();
    private static View view = new View();
    private static Controller controller = new Controller();

    private Controller() {
    }

    static int entering() {
        String number;
        int intNumber;
        Scanner in = new Scanner(System.in);
        number = in.nextLine();
        if (number.contains("^[a-zA-Z]+")) {
            System.out.println("Try again.");
            intNumber = -1;
        } else {
            intNumber = Integer.parseInt(number);
        }
        return intNumber;
    }

    public static void main(final String[] args) {
        int result = -1;
        view.greating();
        view.printAll();
        do {
            result = controller.entering();
        } while (result <= -1);
        if (result == 1) {
            System.out.println(market.addProducts());
        }

        view.printOneKindOfProducts();
        do {
            result = controller.entering();
        } while (result <= -1);
        if (result == 1) {
            System.out.println(market.searchingOneKindProducts("Wooden"));
        } else if (result == 2) {
            System.out.println(market.searchingOneKindProducts("Plumbing"));
        } else if (result == 3) {
            System.out.println(market.searchingOneKindProducts("Paint"));
        }
        view.printFromTheSmallestPrice();
        do {
            result = controller.entering();
        } while (result <= -1);
        if (result == 1) {

            for (int i = 0;i <=market.sortingFromTheSmallestPrice().length-1; i++) {
                System.out.println(market.sortingFromTheSmallestPrice()[i] + " ");
            }
        }
        view.printFromTheBiggestPrice();
        do {
            result = controller.entering();
        } while (result <= -1);
        if (result == 1) {
            market.sortingFromTheBiggestPrice();
        }

        view.printProductsWithFixedPrice();
        do {
            result = controller.entering();
        } while (result <= -1);
        System.out.println("Enter price which you want: ");
        do {
            result = controller.entering();
        } while (result <= -1);
        System.out.println(market.searchingFixedPrice(result));
    }

}
